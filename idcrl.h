#pragma once

#include "stdafx.h"

struct PIH;

typedef PIH *PassportIdentityHandle;

typedef HRESULT(WINAPI *IdentityChangedCallback)(IN PassportIdentityHandle hIdentity, IN void * pvVoid, IN bool bCanContinue);

#define TOKEN_MAX_LEN 200

struct PIH {
	LPCWSTR credMemberName;
	LPCWSTR credMemberPass;
	WCHAR token[TOKEN_MAX_LEN];
	bool success;
	HANDLE logonThread;

	IdentityChangedCallback cb;
	void* cbData;
};

typedef struct {
	DWORD cbSize;
	LPCWSTR wzServiceTarget;
	LPCWSTR wzServicePolicy;
	DWORD dwTokenFlags;
	DWORD dwTokenParam;
} RSTParams, *PRSTParams;

typedef const PRSTParams PCRSTParams;

#define MEMBER_NAME_MAX_LENGTH 100

struct IdentityEntry {
	WCHAR szMemberName[MEMBER_NAME_MAX_LENGTH];
	IdentityEntry* next;
};

struct PEIH {
	IdentityEntry* root;
	IdentityEntry* current;
};

typedef PEIH *PassportEnumIdentitiesHandle;

const HRESULT PPCRL_S_NO_MORE_IDENTITIES = 0x48860;
const HRESULT PPCRL_REQUEST_E_INVALID_MEMBER_NAME = 0x8004882D;
const HRESULT PPCRL_REQUEST_E_BAD_MEMBER_NAME_OR_PASSWORD = 0x80048821;
const HRESULT PPCRL_REQUEST_E_MISSING_PRIMARY_CREDENTIAL = 0x8004882E;
const HRESULT PPCRL_E_UNABLE_TO_RETRIEVE_SERVICE_TOKEN = 0x80048862;
const HRESULT PPCRL_AUTHSTATE_E_UNAUTHENTICATED = 0x80048800;

typedef enum {
	IDCRL_REQUEST_BUILD_ERROR = 0x00000001,
	IDCRL_REQUEST_SEND_ERROR = 0x00000002,
	IDCRL_RESPONSE_RECEIVE_ERROR = 0x00000003,
	IDCRL_RESPONSE_READ_ERROR = 0x00000004,
	IDCRL_REPSONSE_PARSE_ERROR = 0x00000005,
	IDCRL_RESPONSE_SIG_DECRYPT_ERROR = 0x00000006,
	IDCRL_RESPONSE_PARSE_HEADER_ERROR = 0x00000007,
	IDCRL_RESPONSE_PARSE_TOKEN_ERROR = 0x00000008,
	IDCRL_RESPONSE_PUTCERT_ERROR = 0x00000009
} IDCRL_ERROR_CATEGORY;

typedef struct {
	DWORD dwId;
	BYTE * pValue;
	size_t cbValue;
} IDCRL_OPTION, *LPIDCRL_OPTION;
