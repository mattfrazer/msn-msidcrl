#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <WinInet.h>
#include <WinCrypt.h>
#include <cstdio>
#include <cstdlib>
